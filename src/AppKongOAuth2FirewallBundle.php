<?php

namespace App\Bundle\KongOAuth2FirewallBundle;

use App\Bundle\KongOAuth2FirewallBundle\Security\Factory\KongFactory;
use App\Bundle\KongOAuth2FirewallBundle\Security\HeaderAuthenticator;
use App\Bundle\KongOAuth2FirewallBundle\Security\TrustedUserProvider;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @author Quentin Schuler <q.schuler@wakeonweb.com>
 */
class AppKongOAuth2FirewallBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new KongFactory());
    }
}
